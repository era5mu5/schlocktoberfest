<?php

namespace App\Views;

class MerchandiseView extends TemplateView
{
        
    public function render()
    {
        
        extract($this->data);

        $page = "merchandise";
        $page_title = "merchandise";

        include "templates/master.inc.php";
    }

    public function content()
    {
        extract($this->data);
        include "templates/merchandise.inc.php";
    }
}
