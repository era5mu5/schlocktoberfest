<?php

namespace App\Views;

class MovieSuggestSuccessView
{

    public function render()
    {
        $page = "moviesuggestsuccess";
        $page_title = "Movie Suggest Success";
        include "templates/master.inc.php";
    }

        public function content()
        {
        include "templates/moviesuggestsuccess.inc.php";
        }
}
