<?php

namespace App\Controllers;

use App\Models\Merchandise;
use App\Views\MerchandiseView;

class MerchandiseController extends Controller
{
    public function show()
    {
        $merchandise = Merchandise::all();

        $view = new MerchandiseView(['merchandise' => $merchandise]);
        $view->render();
    }
}
